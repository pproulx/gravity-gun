using UnityEngine;
using System.Collections;

public class CursorLock : MonoBehaviour 
{
    private void Update()
	{
		if (Input.GetMouseButtonDown(0))
		{
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;
		}
		
        if (Input.GetKeyDown("escape"))
		{
			Cursor.lockState = CursorLockMode.None;
			Cursor.visible = true;
		}
    }
}
