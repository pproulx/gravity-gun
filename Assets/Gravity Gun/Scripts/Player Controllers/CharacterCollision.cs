using UnityEngine;
using System.Collections;

public class CharacterCollision : MonoBehaviour 
{
	private const float MINIMUM_Y_HIT_DIRECTION = -0.3f;
	[SerializeField] private float pushPower = 2f;
    
	private void OnControllerColliderHit(ControllerColliderHit hit) 
	{
        Rigidbody body = hit.collider.attachedRigidbody;

		if (!(body == null || body.isKinematic || hit.moveDirection.y < MINIMUM_Y_HIT_DIRECTION))
		{
			Vector3 pushDir = new Vector3(hit.moveDirection.x, 0, hit.moveDirection.z);
			body.velocity = pushDir * pushPower;
		}
    }
}
