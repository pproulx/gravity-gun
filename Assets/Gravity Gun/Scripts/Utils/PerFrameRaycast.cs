using UnityEngine;
using System.Collections;

public class PerFrameRaycast : MonoBehaviour
{
	[SerializeField] private Transform raycastOrigin;
	private RaycastHit hitInfo = new RaycastHit ();

	public RaycastHit HitInfo
	{
		get { return hitInfo; }
	}

	private void Update () 
	{
		Physics.Raycast (raycastOrigin.position + (raycastOrigin.forward * 0.25f) , raycastOrigin.forward, out hitInfo, 100, Layer.NOT_PLAYER_MASK);
	}
}