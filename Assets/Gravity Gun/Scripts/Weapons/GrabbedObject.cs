using UnityEngine;
using System.Collections;

public class GrabbedObject : MonoBehaviour 
{
	[System.NonSerialized] public GravityGun gravityGun;
	[System.NonSerialized] public new Rigidbody rigidbody;

	private void Awake()
	{
		rigidbody = GetComponent<Rigidbody>();
	}

    private void OnCollisionEnter(Collision collision)
	{
        if (collision.relativeVelocity.magnitude > gravityGun.breakGrabSensibility)
		{
			gravityGun.Release(GravityGun.ReleaseReason.Break);
		}
    }
}
