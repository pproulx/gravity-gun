using UnityEngine;
using System.Collections;

public class GravityGun : MonoBehaviour 
{
	public enum ReleaseReason {Player, Break, AntiSurf};

	[SerializeField] private Transform attractionCenter;
	
	[SerializeField] private float range = 5;
	[SerializeField] private float force = 15;
	[SerializeField] private float angularForce = 4;
	public float breakGrabSensibility = 4;
	
	[SerializeField] private float releaseMaxVelocity = 3;
	
	[SerializeField] private float antiSurfReleaseDelay = 0.75f;
	[SerializeField] private float grabLockDelay = 0.01f;
	[SerializeField] private float grabDistanceDeltaRatio = 2.5f;
	
	private PerFrameRaycast raycast;

	private GrabbedObject grabbedObject;
	
	private float grabTime;
	
	#region Anti-Surf
	private float grabbedDistancePreviousFrame;
	private bool grabLocked = false;
	private float grabAttemptTime = Mathf.NegativeInfinity;		//If the grab does not lock, the attempt fail, else, the grabAttempTime is reseted to -Infinity;
	#endregion

	private void Awake ()
	{
		raycast = GetComponent<PerFrameRaycast>();
	}

	private void Update () 
	{
		Transform targetedObject = raycast.HitInfo.transform;
		
		if (Input.GetMouseButtonDown(0) && 
		    targetedObject != null && 
		    targetedObject.gameObject.layer == Layer.BOX && 
		    raycast.HitInfo.distance < range)
		{
			//Anti-Surf - avoid frenzy click (only when grab was not locked)
			if (Time.timeSinceLevelLoad - grabAttemptTime < antiSurfReleaseDelay)
			{
				return;
			}

			Grab(targetedObject);
		}

		if (Input.GetMouseButtonUp(0) && grabbedObject)
		{
			Release(ReleaseReason.Player);
		}
		
		RotateGrabbedObject();
	}
	
	private void FixedUpdate()
	{
		if (grabbedObject)
		{
			MoveGrabbedObject();
			TryLockAndAntiSurf();
		}
	}
	
	private void Grab(Transform targetedObject)
	{
		grabTime = Time.timeSinceLevelLoad;
		grabAttemptTime = Time.timeSinceLevelLoad;
	
		grabbedObject = targetedObject.gameObject.AddComponent<GrabbedObject>();
		grabbedObject.gravityGun = this;

		grabbedObject.rigidbody.useGravity = false;

		grabbedDistancePreviousFrame = Mathf.Infinity;
	}

	private void RotateGrabbedObject()
	{
		if (grabbedObject)
		{
			UpdateAttractionCenterRotation(grabbedObject.transform);
			
			grabbedObject.transform.rotation = 
				Quaternion.Slerp(grabbedObject.transform.rotation,
			                     attractionCenter.rotation, 
			                     Time.deltaTime * angularForce * (Mathf.Min(Time.timeSinceLevelLoad - grabTime, 1f)));    
		}
	}

	private void UpdateAttractionCenterRotation(Transform targetedObject)
	{
		Quaternion targetedObjectRotation = targetedObject.rotation;
		Quaternion newAttractionCenterRotation = transform.rotation;
		
		doY(targetedObjectRotation, ref newAttractionCenterRotation);
		doX(targetedObjectRotation, ref newAttractionCenterRotation);
		doZ(targetedObjectRotation, ref newAttractionCenterRotation);
		
		attractionCenter.rotation = newAttractionCenterRotation;
	}

	private void MoveGrabbedObject()
	{
		Vector3 diff = attractionCenter.position - grabbedObject.rigidbody.position;
		Vector3 targetVelocity = diff * force;
		grabbedObject.rigidbody.velocity = targetVelocity;
		grabbedObject.rigidbody.angularVelocity = Vector3.zero;
	}
	
	public void Release(ReleaseReason releaseReason)
	{
		if (grabbedObject)
		{
			Vector3 newVelocity = grabbedObject.rigidbody.velocity;
			newVelocity.x = Mathf.Clamp(newVelocity.x, -releaseMaxVelocity, releaseMaxVelocity);
			newVelocity.y = Mathf.Clamp(newVelocity.y, -releaseMaxVelocity, releaseMaxVelocity);
			newVelocity.z = Mathf.Clamp(newVelocity.z, -releaseMaxVelocity, releaseMaxVelocity);
			grabbedObject.rigidbody.velocity = newVelocity;
				
			grabbedObject.rigidbody.useGravity = true;

			DestroyObject(grabbedObject);
		
			grabbedObject = null;
			grabLocked = false;
			
			attractionCenter.LookAt(transform);
			attractionCenter.Rotate(new Vector3(0, 180, 0));
		}
	}

	private void TryLockAndAntiSurf()
	{
		float grabbedDistance = Vector3.Distance(grabbedObject.transform.position, Camera.main.transform.position) - 2;
		
		if (!grabLocked && Mathf.Abs(grabbedDistance) < grabLockDelay)
		{
			grabLocked = true;
			grabAttemptTime = Mathf.NegativeInfinity;
		}
		
		float grabedDistanceDelta = grabbedDistancePreviousFrame - grabbedDistance;
		
		if (!grabLocked && grabbedDistance > 0 && grabbedDistance / grabedDistanceDelta > grabDistanceDeltaRatio)
		{
			Release(ReleaseReason.AntiSurf);
			return;
		}
		
		grabbedDistancePreviousFrame = grabbedDistance;
	}

	//Todo: This could by abstracted ?
	private void doX(Quaternion fromRotation, ref Quaternion toRotation)
	{
		float rotDiff = Mathf.Repeat(toRotation.eulerAngles.x - fromRotation.eulerAngles.x, 360);
		
		if (rotDiff >= 45 && rotDiff < 135)
		{
			toRotation *= Quaternion.Euler(270, 0, 0);
		} 
		else if (rotDiff >= 135 && rotDiff < 225)
		{
			toRotation *= Quaternion.Euler(180, 0, 0);
		} 
		else if (rotDiff >= 225 && rotDiff < 315)
		{
			toRotation *= Quaternion.Euler(90, 0, 0);
		} 
		else if (rotDiff >= 315 || rotDiff < 45)
		{
			toRotation *= Quaternion.Euler(0, 0, 0);
		}
	}
	
	private void doY(Quaternion fromRotation, ref Quaternion toRotation)
	{
		float rotDiff = Mathf.Repeat(toRotation.eulerAngles.y - fromRotation.eulerAngles.y, 360);
		
		if (rotDiff >= 45 && rotDiff < 135)
		{
			toRotation *= Quaternion.Euler(0, 270, 0);
		} else if (rotDiff >= 135 && rotDiff < 225)
		{
			toRotation *= Quaternion.Euler(0, 180, 0);
		} else if (rotDiff >= 225 && rotDiff < 315)
		{
			toRotation *= Quaternion.Euler(0, 90, 0);
		} else if (rotDiff >= 315 || rotDiff < 45)
		{
			toRotation *= Quaternion.Euler(0, 0, 0);
		}
	}

	private void doZ(Quaternion fromRotation, ref Quaternion toRotation)
	{
		float rotDiff = Mathf.Repeat(toRotation.eulerAngles.z - fromRotation.eulerAngles.z, 360);
		
		if (rotDiff >= 45 && rotDiff < 135)
		{
			toRotation *= Quaternion.Euler(0, 0, 270);
		} else if (rotDiff >= 135 && rotDiff < 225)
		{
			toRotation *= Quaternion.Euler(0, 0, 180);
		} else if (rotDiff >= 225 && rotDiff < 315)
		{
			toRotation *= Quaternion.Euler(0, 0, 90);	
		} else if (rotDiff >= 315 || rotDiff < 45)
		{
			toRotation *= Quaternion.Euler(0, 0, 0);
		}
	}
}
