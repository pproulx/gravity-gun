using UnityEngine;
using System.Collections;

public class Layer : MonoBehaviour 
{
	public const int PLAYER = 8;
	public const int NOT_PLAYER_MASK = ~(1 << Layer.PLAYER);
	public const int BOX    = 9;
}
